import withProgressReadingBar from './withProgressReadingBar';
import React from 'react';
import { shallow } from 'enzyme';
import Lorem from '../components/Lorem';

describe('LoremWithProgressReadingBar', () => {
  it('should return component', () => {
    const LoremWPRB = withProgressReadingBar(Lorem);
    const component = shallow(<LoremWPRB />);
    expect(component.html()).not.toBe(null);
  });

  it('should have 2 divs in it', () => {
    const LoremWPRB = withProgressReadingBar(Lorem);
    const component = shallow(<LoremWPRB />);
    expect(component.find('div')).toHaveLength(2);
  });
});
