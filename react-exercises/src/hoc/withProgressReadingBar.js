import React, { useEffect, useRef, useState } from 'react';
import './withProgressReadingBar.css';

const withProgressReadingBar = (Component) => (props) => {
  const childWrapper = useRef(null);
  const [barWidth, setBarWidth] = useState(0);

  useEffect(() => {
    const calculateBarWidth = () => {
      const isOnTop =
        window.pageYOffset > childWrapper.current.offsetTop &&
        window.pageYOffset <
          childWrapper.current.offsetTop + childWrapper.current.offsetHeight;

      if (isOnTop) {
        setBarWidth(
          ((window.pageYOffset - childWrapper.current.offsetTop) /
            childWrapper.current.offsetHeight) *
            100
        );
      } else {
        setBarWidth(0);
      }
    };
    calculateBarWidth();
    document.addEventListener('scroll', calculateBarWidth);
    return () => {
      document.removeEventListener('scroll', calculateBarWidth);
    };
  });
  return (
    <>
      <div className="progress-reading-bar" style={{ width: barWidth + '%' }} />
      <div
        ref={childWrapper}
        style={{ maxHeight: 50 + 'vh', overflow: 'hidden' }}
      >
        <Component />
      </div>
    </>
  );
};
export default withProgressReadingBar;
