import React from 'react';
import Lorem from './components/Lorem';
import withProgressReadingBar from './hoc/withProgressReadingBar';

const LoremWPRB = withProgressReadingBar(Lorem);
function App() {
  return (
    <div className="App">
      <LoremWPRB />
      <Lorem />
    </div>
  );
}

export default App;
