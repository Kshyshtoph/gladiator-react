import React from 'react';
const Lorem = () => {
  return (
    <p>
      Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aspernatur,
      reiciendis. Sint quia, qui nisi tenetur error repellat ratione earum nemo
      labore ullam beatae reprehenderit necessitatibus laborum quasi asperiores
      sunt distinctio! Velit adipisci vel molestiae, commodi officiis distinctio
      enim minima nesciunt voluptates? Recusandae dolorem, amet sunt harum,
      laboriosam magni fuga cupiditate rerum maiores, quis a consectetur
      temporibus veniam iusto culpa perferendis? Eos illum modi repudiandae
      ipsam. Recusandae accusantium nulla quia ipsa, delectus dolores dolor
      similique iure? Facere nobis sapiente sunt odio voluptates, amet deleniti,
      officiis nisi minus inventore possimus autem expedita. Eaque eveniet
      pariatur praesentium ullam vero voluptate fugiat non sed, cupiditate iure
      dolorem similique id nulla a impedit earum quos totam quam omnis dicta eum
      corrupti. Placeat quas consequatur magnam. Aperiam neque sit modi sint
      eligendi cum labore possimus a corrupti corporis dolore animi delectus
      nostrum cupiditate maiores voluptatum, voluptatibus, voluptate ad
      reiciendis. Inventore accusantium quam tempore officiis suscipit quae.
      Consequuntur deleniti rem praesentium! Qui praesentium vitae eligendi eius
      ex saepe natus perferendis eos illo. Quas facere explicabo atque
      reiciendis error dolor, qui magnam, repudiandae fugiat, esse cumque in
      distinctio? Tempora assumenda ullam officia eveniet esse aliquid
      consequuntur, perferendis asperiores quam doloribus labore laudantium
      laborum beatae numquam aspernatur recusandae eaque facere id natus cum
      ipsum. Beatae quos quam tenetur velit. Deleniti sequi maxime aliquid.
      Repellat ea, ipsum laboriosam quas quae quaerat reiciendis incidunt
      eligendi similique asperiores illum numquam accusamus. Eveniet ratione ea
      iste nobis magni. Molestiae blanditiis accusantium eos ut? Pariatur
      impedit illum magnam aperiam neque architecto voluptates fugiat sit,
      praesentium voluptas? Dolorem, laboriosam? Perspiciatis architecto, quod
      consequuntur quis doloremque nam incidunt. Ipsam id incidunt facilis iure,
      ipsum rem itaque. Animi architecto, in neque aut molestiae ea quibusdam,
      temporibus fugiat porro assumenda aspernatur iure numquam, quis tenetur.
      Architecto aspernatur deleniti facilis sed molestiae, tempore est sequi
      illum esse repellat vitae?
    </p>
  );
};
export default Lorem;
