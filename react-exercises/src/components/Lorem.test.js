import React from 'react';
import { shallow } from 'enzyme';
import Lorem from './Lorem';

describe('Lorem element', () => {
  it('contains one p element', () => {
    const lorem = shallow(<Lorem />);
    expect(lorem.find('p')).toHaveLength(1);
  });
});
